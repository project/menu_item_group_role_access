Menu Item Group Role Access Module

Introduction
------------

Welcome to the Menu Item Group Role Access module! 
This Drupal module enhances your website's access control by 
allowing you to restrict access to menu items based on the group 
roles assigned to a user within the group they belong to. 
This functionality adds an extra layer of security and customization 
to your Drupal site, ensuring that users with specific roles 
within a group can access designated menu items.

Requirements
------------

Before you get started with this module, make sure you have the 
following modules installed:

 * menu_link_content
 * menu_ui
 * group

Please ensure that these modules are installed and enabled on 
your Drupal installation.

Installation
------------

 * Install as you would normally install a contributed Drupal module. 
  See: https://www.drupal.org/docs/extending-drupal/installing-modules
   for further information.

CONFIGURATION
-------------

Once the module is installed, configuring it is a straightforward process:

 * Navigate to the admin UI and go to the menu item you wish to 
   restrict access to.
 * Select the group roles that should have access to the menu item. 
   This configuration step allows you to define precisely 
   which roles within a group can access specific menu items.

MAINTAINERS
-----------
This module is actively maintained by a dedicated team of developers. 
If you have any questions, concerns, or issues, 
feel free to reach out to our maintainers.

 * Sunil kumar
    * Drupal.org Profile: https://www.drupal.org/u/sunil_lnwebworks
 * Shikha Dawar
    * Drupal.org Profile: https://www.drupal.org/u/shikha_lnweb
 * Pankaj kumar
    * Drupal.org Profile: https://www.drupal.org/u/pankaj_lnweb
 * Manpreet Singh
    * Drupal.org Profile: https://www.drupal.org/u/manpreet_singh
